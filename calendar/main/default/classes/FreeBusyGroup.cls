/**
 * Model definition for FreeBusyGroup.
 *
 * <p> This is the Java data model class that specifies how to parse/serialize into the JSON that is
 * transmitted over HTTP when working with the Calendar API. For a detailed explanation see:
 * <a href="https://developers.google.com/api-client-library/java/google-http-java-client/json">https://developers.google.com/api-client-library/java/google-http-java-client/json</a>
 * </p>
 *
 * @author Google, Inc.
 */
 public class FreeBusyGroup {

  /**
   * List of calendars' identifiers within a group.
   * The value may be {@code null}.
   */
  public List<String> calendars{get;set;}

  /**
   * Optional error(s) (if computation for the group failed).
   * The value may be {@code null}.
   */
  private List<Error> errors{get;set;}

}
