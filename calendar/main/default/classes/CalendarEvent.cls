public class CalendarEvent {
    public CalendarEvent() {
        this.visibility='private';
        this.start=new EventDateTime();
        this.end2=new EventDateTime();
        this.attendees=new List<EventAttendee>();
    }

        /**
        * Whether anyone can invite themselves to the event (currently works for Google+ events only).
        * Optional. The default is False.
        * The value may be {@code null}.
        */
        //public Boolean anyoneCanAddSelf{get;set;}

        /**
        * File attachments for the event. Currently only Google Drive attachments are supported. In order
        * to modify attachments the supportsAttachments request parameter should be set to true. There
        * can be at most 25 attachments per event,
        * The value may be {@code null}.
        */
        //private List<EventAttachment> attachments;

        /**
        * The attendees of the event. See the Events with attendees guide for more information on
        * scheduling events with other calendar users.
        * The value may be {@code null}.
        */
        public List<EventAttendee> attendees{get;set;}

        /**
        * Whether attendees may have been omitted from the event's representation. When retrieving an
        * event, this may be due to a restriction specified by the maxAttendee query parameter. When
        * updating an event, this can be used to only update the participant's response. Optional. The
        * default is False.
        * The value may be {@code null}.
        */
        //public Boolean attendeesOmitted{get;set;}

        /**
        * The color of the event. This is an ID referring to an entry in the event section of the colors
        * definition (see the  colors endpoint). Optional.
        * The value may be {@code null}.
        */
        //public String colorId{get;set;}

        /**
        * The conference-related information, such as details of a Hangouts Meet conference. To create
        * new conference details use the createRequest field. To persist your changes, remember to set
        * the conferenceDataVersion request parameter to 1 for all event modification requests.
        * The value may be {@code null}.
        */
        //private ConferenceData conferenceData;

        /**
        * Creation time of the event (as a RFC3339 timestamp). Read-only.
        * The value may be {@code null}.
        */
        //private com.google.api.client.util.DateTime created;

        /**
        * The creator of the event. Read-only.
        * The value may be {@code null}.
        */
        //
        //public Creator creator;

        /**
        * Description of the event. Optional.
        * The value may be {@code null}.
        */
        //public String description{get;set;}

        /**
        * The (exclusive) end time of the event. For a recurring event, this is the end time of the first
        * instance.
        * The value may be {@code null}.
        */
        public EventDateTime end2;//HACK:

        /**
        * Whether the end time is actually unspecified. An end time is still provided for compatibility
        * reasons, even if this attribute is set to True. The default is False.
        * The value may be {@code null}.
        */
        //public Boolean endTimeUnspecified;

        /**
        * ETag of the resource.
        * The value may be {@code null}.
        */
        public String etag{get;set;}

        /**
        * Extended properties of the event.
        * The value may be {@code null}.
        */
        //private ExtendedProperties extendedProperties;

        /**
        * A gadget that extends this event.
        * The value may be {@code null}.
        */
        //private Gadget gadget;

        /**
        * Whether attendees other than the organizer can invite others to the event. Optional. The
        * default is True.
        * The value may be {@code null}.
        */
        //public Boolean guestsCanInviteOthers;

        /**
        * Whether attendees other than the organizer can modify the event. Optional. The default is
        * False.
        * The value may be {@code null}.
        */
        //public Boolean guestsCanModify;

        /**
        * Whether attendees other than the organizer can see who the event's attendees are. Optional. The
        * default is True.
        * The value may be {@code null}.
        */
        //public Boolean guestsCanSeeOtherGuests;

        /**
        * An absolute link to the Google+ hangout associated with this event. Read-only.
        * The value may be {@code null}.
        */
        //public String hangoutLink;

        /**
        * An absolute link to this event in the Google Calendar Web UI. Read-only.
        * The value may be {@code null}.
        */
        public String htmlLink{get;set;}

        /**
        * Event unique identifier as defined in RFC5545. It is used to uniquely identify events accross
        * calendaring systems and must be supplied when importing events via the import method. Note that
        * the icalUID and the id are not identical and only one of them should be supplied at event
        * creation time. One difference in their semantics is that in recurring events, all occurrences
        * of one event have different ids while they all share the same icalUIDs.
        * The value may be {@code null}.
        */
        //public String iCalUID;

        /**
        * Opaque identifier of the event. When creating new single or recurring events, you can specify
        * their IDs. Provided IDs must follow these rules: - characters allowed in the ID are those used
        * in base32hex encoding, i.e. lowercase letters a-v and digits 0-9, see section 3.1.2 in RFC2938
        * - the length of the ID must be between 5 and 1024 characters  - the ID must be unique per
        * calendar  Due to the globally distributed nature of the system, we cannot guarantee that ID
        * collisions will be detected at event creation time. To minimize the risk of collisions we
        * recommend using an established UUID algorithm such as one described in RFC4122. If you do not
        * specify an ID, it will be automatically generated by the server. Note that the icalUID and the
        * id are not identical and only one of them should be supplied at event creation time. One
        * difference in their semantics is that in recurring events, all occurrences of one event have
        * different ids while they all share the same icalUIDs.
        * The value may be {@code null}.
        */
        public String id{get;set;}

        /**
        * Type of the resource ("calendar#event").
        * The value may be {@code null}.
        */
        //public String kind{get;set;}

        /**
        * Geographic location of the event as free-form text. Optional.
        * The value may be {@code null}.
        */
        //public String location{get;set;}

        /**
        * Whether this is a locked event copy where no changes can be made to the main event fields
        * "summary", "description", "location", "start", "end" or "recurrence". The default is False.
        * Read-Only.
        * The value may be {@code null}.
        */
        //public Boolean locked;

        /**
        * The organizer of the event. If the organizer is also an attendee, this is indicated with a
        * separate entry in attendees with the organizer field set to True. To change the organizer, use
        * the move operation. Read-only, except when importing an event.
        * The value may be {@code null}.
        */
        //private Organizer organizer;

        /**
        * For an instance of a recurring event, this is the time at which this event would start
        * according to the recurrence data in the recurring event identified by recurringEventId.
        * Immutable.
        * The value may be {@code null}.
        */
        //private DateTime originalStartTime;

        /**
        * Whether this is a private event copy where changes are not shared with other copies on other
        * calendars. Optional. Immutable. The default is False.
        * The value may be {@code null}.
        */
        //public Boolean privateCopy;

        /**
        * List of RRULE, EXRULE, RDATE and EXDATE lines for a recurring event, as specified in RFC5545.
        * Note that DTSTART and DTEND lines are not allowed in this field; event start and end times are
        * specified in the start and end fields. This field is omitted for single events or instances of
        * recurring events.
        * The value may be {@code null}.
        */
        //private java.util.List<java.lang.String> recurrence;

        /**
        * For an instance of a recurring event, this is the id of the recurring event to which this
        * instance belongs. Immutable.
        * The value may be {@code null}.
        */
        //public String recurringEventId;

        /**
        * Information about the event's reminders for the authenticated user.
        * The value may be {@code null}.
        */
    //    public Reminders reminders{get;set;}

        /**
        * Sequence number as per iCalendar.
        * The value may be {@code null}.
        */
        //public Integer sequence;

        /**
        * Source from which the event was created. For example, a web page, an email message or any
        * document identifiable by an URL with HTTP or HTTPS scheme. Can only be seen or modified by the
        * creator of the event.
        * The value may be {@code null}.
        */
        //private Source source;

        /**
        * The (inclusive) start time of the event. For a recurring event, this is the start time of the
        * first instance.
        * The value may be {@code null}.
        */
        public EventDateTime start{get;set;}

        /**
        * Status of the event. Optional. Possible values are: - "confirmed" - The event is confirmed.
        * This is the default status.  - "tentative" - The event is tentatively confirmed.  - "cancelled"
        * - The event is cancelled.
        * The value may be {@code null}.
        */
        //public String status{get;set;}

        /**
        * Title of the event.
        * The value may be {@code null}.
        */
        public String summary{get;set;}

        /**
        * Whether the event blocks time on the calendar. Optional. Possible values are: - "opaque" -
        * Default value. The event does block time on the calendar. This is equivalent to setting Show me
        * as to Busy in the Calendar UI.  - "transparent" - The event does not block time on the
        * calendar. This is equivalent to setting Show me as to Available in the Calendar UI.
        * The value may be {@code null}.
        */
        //public String transparency;

        /**
        * Last modification time of the event (as a RFC3339 timestamp). Read-only.
        * The value may be {@code null}.
        */
        //private com.google.api.client.util.DateTime updated;

        /**
        * Visibility of the event. Optional. Possible values are: - "default" - Uses the default
        * visibility for events on the calendar. This is the default value.  - "public" - The event is
        * public and event details are visible to all readers of the calendar.  - "private" - The event
        * is private and only event attendees may view event details.  - "confidential" - The event is
        * private. This value is provided for compatibility reasons.
        * The value may be {@code null}.
        */
        public String visibility{get;set;}//=private


    /**
    * The creator of the event. Read-only.
    */
    public class Creator  {

        /**
        * The creator's name, if available.
        * The value may be {@code null}.
        */
        public String displayName{get;set;}

        /**
        * The creator's email address, if available.
        * The value may be {@code null}.
        */
        public String email{get;set;}

        /**
        * The creator's Profile ID, if available. It corresponds to theid field in the People collection
        * of the Google+ API
        * The value may be {@code null}.
        */
        public String id{get;set;}

        /**
        * Whether the creator corresponds to the calendar on which this copy of the event appears. Read-
        * only. The default is False.
        * The value may be {@code null}.
        */
        public Boolean self{get;set;}

    }

    /**
    * Extended properties of the event.
    */
    //  public static final class ExtendedProperties {
    //    /**
    //     * Properties that are private to the copy of the event that appears on this calendar.
    //     * The value may be {@code null}.
    //     */
    //    //public Map<String, String> private;
    //
    //    /**
    //     * Properties that are shared between copies of the event on other attendees' calendars.
    //     * The value may be {@code null}.
    //     */
    //    private Map<String, String> shared;
    //  }

    /**
    * A gadget that extends this event.
    */

    //  public static final class Gadget extends com.google.api.client.json.GenericJson {
    //
    //    /**
    //     * The gadget's display mode. Optional. Possible values are: - "icon" - The gadget displays next
    //     * to the event's title in the calendar view.  - "chip" - The gadget displays when the event is
    //     * clicked.
    //     * The value may be {@code null}.
    //     */
    //    public String display{get;set;}
    //
    //    /**
    //     * The gadget's height in pixels. The height must be an integer greater than 0. Optional.
    //     * The value may be {@code null}.
    //     */
    //     public Integer height{get;set;}
    //
    //    /**
    //     * The gadget's icon URL. The URL scheme must be HTTPS.
    //     * The value may be {@code null}.
    //     */
    //    
    //    public String iconLink;
    //
    //    /**
    //     * The gadget's URL. The URL scheme must be HTTPS.
    //     * The value may be {@code null}.
    //     */
    //    
    //    public String link;
    //
    //    /**
    //     * Preferences.
    //     * The value may be {@code null}.
    //     */
    //    
    //    private java.util.Map<String, java.lang.String> preferences;
    //
    //    /**
    //     * The gadget's title.
    //     * The value may be {@code null}.
    //     */
    //    
        public String title{get;set;}
    //
    //    /**
    //     * The gadget's type.
    //     * The value may be {@code null}.
    //     */
    //    
    //    public String type;
    //
    //    /**
    //     * The gadget's width in pixels. The width must be an integer greater than 0. Optional.
    //     * The value may be {@code null}.
    //     */
    //    
    //    public Integer width;
    //
    //  }

    /**
    * The organizer of the event. If the organizer is also an attendee, this is indicated with a
    * separate entry in attendees with the organizer field set to True. To change the organizer, use
    * the move operation. Read-only, except when importing an event.
    */
    public class Organizer {

        /**
        * The organizer's name, if available.
        * The value may be {@code null}.
        */
        public String displayName{get;set;}

        /**
        * The organizer's email address, if available. It must be a valid email address as per RFC5322.
        * The value may be {@code null}.
        */
        public String email{get;set;}

        /**
        * The organizer's Profile ID, if available. It corresponds to theid field in the People
        * collection of the Google+ API
        * The value may be {@code null}.
        */
        public String id{get;set;}

        /**
        * Whether the organizer corresponds to the calendar on which this copy of the event appears.
        * Read-only. The default is False.
        * The value may be {@code null}.
        */
        public Boolean self{get;set;}

    }

    /**
    * Information about the event's reminders for the authenticated user.
    */
    public class Reminders {

        /**
        * If the event doesn't use the default reminders, this lists the reminders specific to the event,
        * or, if not set, indicates that no reminders are set for this event. The maximum number of
        * override reminders is 5.
        * The value may be {@code null}.
        */
        public List<EventReminder> overrides{get;set;}

        /**
        * Whether the default reminders of the calendar apply to the event.
        * The value may be {@code null}.
        */
        public Boolean useDefault{get;set;}

    }

    public class EventReminder  {

        /**
        * The method used by this reminder. Possible values are: - "email" - Reminders are sent via
        * email.  - "sms" - Reminders are sent via SMS. These are only available for G Suite customers.
        * Requests to set SMS reminders for other account types are ignored.  - "popup" - Reminders are
        * sent via a UI popup.
        * The value may be {@code null}.
        */
        
        public String method{get;set;}

        /**
        * Number of minutes before the start of the event when the reminder should trigger. Valid values
        * are between 0 and 40320 (4 weeks in minutes).
        * The value may be {@code null}.
        */
        
        public Integer minutes{get;set;}
}


    /**
    * Source from which the event was created. For example, a web page, an email message or any
    * document identifiable by an URL with HTTP or HTTPS scheme. Can only be seen or modified by the
    * creator of the event.
    */
    public class Source {

        /**
        * Title of the source; for example a title of a web page or an email subject.
        * The value may be {@code null}.
        */
        public String title{get;set;}

        /**
        * URL of the source pointing to a resource. The URL scheme must be HTTP or HTTPS.
        * The value may be {@code null}.
        */
        public String url{get;set;}
    }


    public class EventAttendee {
        public EventAttendee(String email){
            this.email=email;
        }

        /**
        * Number of additional guests. Optional. The default is 0.
        * The value may be {@code null}.
        */
        //public Integer additionalGuests{get;set;}

        /**
        * The attendee's response comment. Optional.
        * The value may be {@code null}.
        */
        
        //public String comment{get;set;}

        /**
        * The attendee's name, if available. Optional.
        * The value may be {@code null}.
        */
        
        //public String displayName{get;set;}

        /**
        * The attendee's email address, if available. This field must be present when adding an attendee.
        * It must be a valid email address as per RFC5322.
        * The value may be {@code null}.
        */
        
        public String email{get;set;}

        /**
        * The attendee's Profile ID, if available. It corresponds to theid field in the People collection
        * of the Google+ API
        * The value may be {@code null}.
        */
        
        //public String id{get;set;}

        /**
        * Whether this is an optional attendee. Optional. The default is False.
        * The value may be {@code null}.
        */
        
        //public Boolean optional{get;set;}

        /**
        * Whether the attendee is the organizer of the event. Read-only. The default is False.
        * The value may be {@code null}.
        */
        
        //public Boolean organizer{get;set;}

        /**
        * Whether the attendee is a resource. Can only be set when the attendee is added to the event for
        * the first time. Subsequent modifications are ignored. Optional. The default is False.
        * The value may be {@code null}.
        */
        
        //public Boolean resource{get;set;}

        /**
        * The attendee's response status. Possible values are: - "needsAction" - The attendee has not
        * responded to the invitation.  - "declined" - The attendee has declined the invitation.  -
        * "tentative" - The attendee has tentatively accepted the invitation.  - "accepted" - The
        * attendee has accepted the invitation.
        * The value may be {@code null}.
        */
        
        //public String responseStatus{get;set;}

        /**
        * Whether this entry represents the calendar on which this copy of the event appears. Read-only.
        * The default is False.
        * The value may be {@code null}.
        */
        
        //public Boolean self{get;set;}

    }
        
    /**
    * Model definition for EventDateTime.
    *
    * <p> This is the Java data model class that specifies how to parse/serialize into the JSON that is
    * transmitted over HTTP when working with the Calendar API. For a detailed explanation see:
    * <a href="https://developers.google.com/api-client-library/java/google-http-java-client/json">https://developers.google.com/api-client-library/java/google-http-java-client/json</a>
    * </p>
    *
    * @author Google, Inc.
    */
    public class EventDateTime {

        /**
        * The date, in the format "yyyy-mm-dd", if this is an all-day event.
        * The value may be {@code null}.
        */
        //private DateTime date;

        /**
        * The time, as a combined date-time value (formatted according to RFC3339). A time zone offset is
        * required unless a time zone is explicitly specified in timeZone.
        * The value may be {@code null}.
        */
        public DateTime dateTime2{get;set;}//:HACK

        /**
        * The time zone in which the time is specified. (Formatted as an IANA Time Zone Database name,
        * e.g. "Europe/Zurich".) For recurring events this field is required and specifies the time zone
        * in which the recurrence is expanded. For single events this field is optional and indicates a
        * custom time zone for the event start/end.
        * The value may be {@code null}.
        */
        //private String timeZone;

    }
}
