public class CalendarService{

    //Example:
    //FreeBusyResponse r=CalendarService.freeBusyQuery(Id.valueOf('0031700000zFQb9AAG'), DateTime.now(), DateTime.now()+1);
    public static FreeBusyResponse freeBusyQuery(Id conactId, DateTime min,DateTime max){
        TokenResponse resp=OAuthHelper.tokenForContact(conactId);//Id.valueOf('0031700000zFQb9AAG')
        
        Map<String,Object> m=(Map<String,Object> )System.JSON.deserializeUntyped(GoogleOAuthConstants.credentials);
        m=(Map<String,Object> )m.get('web');
        string k=(String)m.get('client_id');
        
        HttpRequest req = new HttpRequest();
        String URL='https://www.googleapis.com/calendar/v3/freeBusy?key='+k;

        req.setEndpoint(URL);
        req.setMethod('POST');
        
        FreebusyRequest fbReq=new FreebusyRequest();
        fbReq.timeMax=max;
        fbReq.timeMin=min;
        fbReq.items.add(new FreeBusyRequestItem('primary'));

        String json=JSON.serialize(fbReq);
        req.setBody(json);
        //Example:
        //'{ "timeMax": "2019-12-31T23:59:00Z", "timeMin": "2019-10-31T23:59:00Z", "items": [ { "id": "primary" } ] }';
        
        req.setHeader('user-agent','Google Calendar API Java Quickstart Google-API-Java-Client');
        req.setHeader('Content-Type', 'application/json; charset=UTF-8' );
        req.setHeader('Authorization',' Bearer '+resp.access_token);
        
        Http http = new Http();
        HTTPResponse res = http.send(req);
        
        if(200==res.getStatusCode()){
            string jsn=res.getBody();
            //Example:
            //'{    "kind": "calendar#freeBusy",    "timeMin": "2019-10-31T23:59:00.000Z",    "timeMax": "2019-12-31T23:59:00.000Z",    "calendars": {        "primary": {            "busy": [                {                    "start": "2019-11-20T16:00:00Z",                    "end": "2019-11-20T18:30:00Z"                },                {                    "start": "2019-12-06T07:30:00Z",                    "end": "2019-12-06T07:45:00Z"                },                {                    "start": "2019-12-06T13:30:00Z",                    "end": "2019-12-06T13:45:00Z"                },                {                    "start": "2019-12-18T09:30:00Z",                    "end": "2019-12-18T10:30:00Z"                }            ]        }    }}';
            
            jsn=jsn.replace('"end":','"end2":');//HACK:end is keyword 

            FreeBusyResponse result=(FreeBusyResponse) System.JSON.deserialize(jsn, FreeBusyResponse.class);

            return result;
        }
        else{
            throw new CalloutException(res.getStatus());
        }
    }
    

    public static CalendarEvent insertEvent(Id contactId,CalendarEvent e){
         Map<String,Object> m=(Map<String,Object> )System.JSON.deserializeUntyped(GoogleOAuthConstants.credentials);
         m=(Map<String,Object> )m.get('web');
         string k=(String)m.get('client_id');
         
         String URL='https://www.googleapis.com/calendar/v3/calendars/primary/events?sendNotifications=true&sendUpdates=all&supportsAttachments=true&key='+k;
 
        string jsn=JSON.serialize(e).replace('"end2":','"end":').replace('"dateTime2":','"dateTime":');
        //Example:
        //'{  "end": {    "dateTime": "2019-12-31T13:59:00Z"  },  "start": {    "dateTime": "2019-12-31T03:59:00Z"  },  "attendees": [    {      "email": "tsadigov@gmail.com"    },    {      "email": "corporate@turalsadik.com"    }  ]}';
 
 
        TokenResponse tkn=OAuthHelper.tokenForContact(contactId);
 
        HttpRequest req = new HttpRequest();
 
        req.setHeader('user-agent','Google Calendar API Java Quickstart Google-API-Java-Client');
        req.setHeader('Content-Type', 'application/json' );
        req.setHeader('Accept', 'application/json' );
        req.setHeader('Authorization',' Bearer '+tkn.access_token);

        req.setEndpoint(URL);
        req.setMethod('POST');
        
        req.setBody(jsn);
        Http http = new Http();
        HTTPResponse res = http.send(req);
        System.debug(res);
        System.debug(res.getBody());
         CalendarEvent result=(CalendarEvent)JSON.deserialize(res.getBody()
                                  .replace('"dateTime":','"dateTime2":')
                                  .replace('"end":','"end2":'), CalendarEvent.class);
        return result;
    }

    /**
    {
 "kind": "calendar#event",
 "etag": "\"3155404679806000\"",
 "id": "q9bl5l3ji5kff82d8st4oaak30",
 "status": "confirmed",
 "htmlLink": "https://www.google.com/calendar/event?eid=cTlibDVsM2ppNWtmZjgyZDhzdDRvYWFrMzAgY29ycG9yYXRlQHR1cmFsc2FkaWsuY29t",
 "created": "2019-12-30T10:38:59.000Z",
 "updated": "2019-12-30T10:38:59.903Z",
 "creator": {
  "email": "corporate@turalsadik.com",
  "self": true
 },
 "organizer": {
  "email": "corporate@turalsadik.com",
  "self": true
 },
 "start": {
  "dateTime": "2019-12-31T06:59:00+03:00"
 },
 "end": {
  "dateTime": "2019-12-31T16:59:00+03:00"
 },
 "iCalUID": "q9bl5l3ji5kff82d8st4oaak30@google.com",
 "sequence": 0,
 "attendees": [
  {
   "email": "tsadigov@gmail.com",
   "responseStatus": "needsAction"
  },
  {
   "email": "corporate@turalsadik.com",
   "organizer": true,
   "self": true,
   "responseStatus": "needsAction"
  }
 ],
 "hangoutLink": "https://meet.google.com/zbz-sakf-krf",
 "conferenceData": {
  "createRequest": {
   "requestId": "d9gvb1jbj4d2s1r41g24kpu458",
   "conferenceSolutionKey": {
    "type": "hangoutsMeet"
   },
   "status": {
    "statusCode": "success"
   }
  },
  "entryPoints": [
   {
    "entryPointType": "video",
    "uri": "https://meet.google.com/zbz-sakf-krf",
    "label": "meet.google.com/zbz-sakf-krf"
   },
   {
    "entryPointType": "more",
    "uri": "https://tel.meet/zbz-sakf-krf?pin=8624916249745",
    "pin": "8624916249745"
   },
   {
    "regionCode": "GB",
    "entryPointType": "phone",
    "uri": "tel:+44-20-3956-2689",
    "label": "+44 20 3956 2689",
    "pin": "525095315"
   }
  ],
  "conferenceSolution": {
   "key": {
    "type": "hangoutsMeet"
   },
   "name": "Hangouts Meet",
   "iconUri": "https://lh5.googleusercontent.com/proxy/bWvYBOb7O03a7HK5iKNEAPoUNPEXH1CHZjuOkiqxHx8OtyVn9sZ6Ktl8hfqBNQUUbCDg6T2unnsHx7RSkCyhrKgHcdoosAW8POQJm_ZEvZU9ZfAE7mZIBGr_tDlF8Z_rSzXcjTffVXg3M46v"
  },
  "conferenceId": "zbz-sakf-krf",
  "signature": "AKipslUskj/C+b2kc00XQa40d9Mh"
 },
 "reminders": {
  "useDefault": true
 }
}

     */
}