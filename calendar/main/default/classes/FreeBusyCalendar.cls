

/**
 * Model definition for FreeBusyCalendar.
 *
 * <p> This is the Java data model class that specifies how to parse/serialize into the JSON that is
 * transmitted over HTTP when working with the Calendar API. For a detailed explanation see:
 * <a href="https://developers.google.com/api-client-library/java/google-http-java-client/json">https://developers.google.com/api-client-library/java/google-http-java-client/json</a>
 * </p>
 *
 * @author Google, Inc.
 */
public  class FreeBusyCalendar  {

  /**
   * List of time ranges during which this calendar should be regarded as busy.
   * The value may be {@code null}.
   */
  public 
  List<TimePeriod> 
  //List<Map<string,Object>>
  busy{get;set;}

  /**
   * Optional error(s) (if computation for the calendar failed).
   * The value may be {@code null}.
   */
  public List<Error> errors{get;set;}

}
