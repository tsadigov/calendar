/**
 * Model definition for TimePeriod.
 *
 * <p> This is the Java data model class that specifies how to parse/serialize into the JSON that is
 * transmitted over HTTP when working with the Calendar API. For a detailed explanation see:
 * <a href="https://developers.google.com/api-client-library/java/google-http-java-client/json">https://developers.google.com/api-client-library/java/google-http-java-client/json</a>
 * </p>
 *
 * @author Google, Inc.
 */
public  class TimePeriod {

  /**
   * The (exclusive) end of the time period.
   * The value may be {@code null}.
   */
  public DateTime end2{get;set;}//end is keyword

  /**
   * The (inclusive) start of the time period.
   * The value may be {@code null}.
   */
  public DateTime start{get;set;}



}
