
/**
 * Model definition for Error.
 *
 * <p> This is the Java data model class that specifies how to parse/serialize into the JSON that is
 * transmitted over HTTP when working with the Calendar API. For a detailed explanation see:
 * <a href="https://developers.google.com/api-client-library/java/google-http-java-client/json">https://developers.google.com/api-client-library/java/google-http-java-client/json</a>
 * </p>
 *
 * @author Google, Inc.
 */
public class Error {

  /**
   * Domain, or broad category, of the error.
   * The value may be {@code null}.
   */
  public String domain{get;set;}

  /**
   * Specific reason for the error. Some of the possible values are: - "groupTooBig" - The group of
   * users requested is too large for a single query.  - "tooManyCalendarsRequested" - The number of
   * calendars requested is too large for a single query.  - "notFound" - The requested resource was
   * not found.  - "internalError" - The API service has encountered an internal error.  Additional
   * error types may be added in the future, so clients should gracefully handle additional error
   * statuses not included in this list.
   * The value may be {@code null}.
   */
  public String reason{get;set;}

}
