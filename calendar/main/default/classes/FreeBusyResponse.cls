
/**
 * Model definition for FreeBusyResponse.
 *
 * <p> This is the Java data model class that specifies how to parse/serialize into the JSON that is
 * transmitted over HTTP when working with the Calendar API. For a detailed explanation see:
 * <a href="https://developers.google.com/api-client-library/java/google-http-java-client/json">https://developers.google.com/api-client-library/java/google-http-java-client/json</a>
 * </p>
 *
 * @author Google, Inc.
 */
public  class FreeBusyResponse {

  /**
   * List of free/busy information for calendars.
   * The value may be {@code null}.
   */
  public Map<String, FreeBusyCalendar> calendars{get;set;}

  /**
   * Expansion of groups.
   * The value may be {@code null}.
   */
  public Map<String, FreeBusyGroup> groups{get;set;}

  /**
   * Type of the resource ("calendar#freeBusy").
   * The value may be {@code null}.
   */
  public String kind{get;set;}

  /**
   * The end of the interval.
   * The value may be {@code null}.
   */
  public DateTime timeMax{get;set;}

  /**
   * The start of the interval.
   * The value may be {@code null}.
   */
  public DateTime timeMin{get;set;}


}

//example
/*
{
    "kind": "calendar#freeBusy",
    "timeMin": "2019-10-31T23:59:00.000Z",
    "timeMax": "2019-12-31T23:59:00.000Z",
    "calendars": {
        "primary": {
            "busy": [
                {
                    "start": "2019-11-20T16:00:00Z",
                    "end": "2019-11-20T18:30:00Z"
                },
                {
                    "start": "2019-12-06T07:30:00Z",
                    "end": "2019-12-06T07:45:00Z"
                },
                {
                    "start": "2019-12-06T13:30:00Z",
                    "end": "2019-12-06T13:45:00Z"
                },
                {
                    "start": "2019-12-18T09:30:00Z",
                    "end": "2019-12-18T10:30:00Z"
                }
            ]
        }
    }
}
*/