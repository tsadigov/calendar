public class FreebusyRequest {
    public DateTime timeMax{get;set;}
    public DateTime timeMin{get;set;}
    public List<FreeBusyRequestItem> items{get;set;} 
    
    public FreebusyRequest() {
        this.items=new List<FreeBusyRequestItem>();
    }
}
